const videoKindCollection = {
  'youtube#video': (videoData) => videoData.id,
  'youtube#searchResult': (videoData) => videoData.id.videoId,
};

export const videoIdExtractor = (videoData) => {
  return (videoKindCollection[videoData.kind] || ((videoData) => videoData.id))(
    videoData
  );
};
