export const shadows = {
  embossElevation:
    '-7px -7px 16px 0 #FFFFFF, 7px 7px 10px -4px rgba(116,150,179,0.27)',
  embossDepression:
    'inset 3px 3px 7px 0 rgba(116,150,179,0.32), inset -4px -4px 6px 0 #FFFFFF',
  cutout_drop_shadow: 'drop-shadow(2.5px 2.5px 5px #777777)',
};

export const globalStyles = {
  colors: {
    containerBase: '#EDF3F9',
    dusk: '#3f3e60',
  },
  desktop: {
    screenBoundaryPadding: 10,
    drawer: {
      base: {
        border: 'none',
        backgroundColor: '#EDF3F9',
        transition: 'all 0.2s linear',
      },
      open: {
        width: 240,
      },
      closed: {
        width: 75,
      },
    },
    menuItem: {
      base: {
        display: 'flex',
        padding: '10px 20px',
        alignItems: 'center',
        width: 'calc(100% - 20px)',
        boxSizing: 'border-box',
        justifyContent: 'flex-start',
        backgroundColor: '#EDF3F9',
        border: 'none',
        textDecoration: 'none',
      },
      icon: {
        fontSize: 25,
        color: '#3f3e60',
        margon: '10px 10px 10px 0px',
      },
    },
    header: {
      base: {
        height: 75,
        backgroundColor: '#EDF3F9',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        transition: 'all 0.2s linear',
      },
    },
    contentArea: {
      base: {
        backgroundColor: '#EDF3F9',
        position: 'absolute',
        borderTopLeftRadius: 15,
        overflowY: 'auto',
        boxShadow: shadows.embossDepression,
        transition: 'all 0.2s linear',
        padding: 20,
      },
    },
  },
  mobile: {},
};
