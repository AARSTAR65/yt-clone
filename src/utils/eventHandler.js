import mitt from 'mitt';

const event = mitt();

export default event;

export const appEvents = {
  AUTH_STATUS_CHANGE: 'AUTH_STATUS_CHANGE',
};
