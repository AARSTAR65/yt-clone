export const MovieTheatreContents = (props) => [
  {
    viewBlockType: 'container',
    profile: 'container_2',
    title: 'Your liked videos',
    content: {
      preload: [],
      APIFetch: {
        contentType: 'videos',
        viewBlockType: 'tile',
        profile: 'tile_2',
        options: {
          id: props.value,
          part: ['snippet'],
          maxResults: 10,
        },
      },
    },
  },
];
