const videoCategories = [
  {
    title: 'Film & Animation',
    id: 1,
  },
  {
    title: 'Autos & Vehicles',
    id: 2,
  },
  {
    title: 'Music',
    id: 10,
  },
  {
    title: 'Pets & Animals',
    id: 15,
  },
  {
    title: 'Sports',
    id: 17,
  },
  {
    title: 'Gaming',
    id: 20,
  },
];

export const CategoriesScreenContents = () =>
  videoCategories.map((category) => ({
    viewBlockType: 'container',
    profile: 'container_2',
    title: category.title,
    content: {
      preload: [],
      APIFetch: {
        contentType: 'videos',
        viewBlockType: 'tile',
        profile: 'tile_1',
        options: {
          chart: 'mostPopular',
          maxResults: 10,
          videoCategoryId: category.id,
        },
      },
    },
  }));
