export const HomeScreenContents = (props) =>
  props.isLoggedIn
    ? [
        {
          viewBlockType: 'container',
          profile: 'container_1',
          title: 'Most Popular',
          content: {
            preload: [],
            APIFetch: {
              contentType: 'videos',
              viewBlockType: 'tile',
              profile: 'tile_1',
              options: {
                chart: 'mostPopular',
                maxResults: 10,
              },
            },
          },
        },
        {
          viewBlockType: 'container',
          profile: 'container_2',
          title: 'Your liked videos',
          content: {
            preload: [],
            APIFetch: {
              contentType: 'videos',
              viewBlockType: 'tile',
              profile: 'tile_1',
              options: {
                myRating: 'like',
                maxResults: 15,
              },
            },
          },
        },
      ]
    : [
        {
          viewBlockType: 'container',
          profile: 'container_2',
          title: 'Most Popular',
          content: {
            preload: [],
            APIFetch: {
              contentType: 'videos',
              viewBlockType: 'tile',
              profile: 'tile_1',
              options: {
                chart: 'mostPopular',
                maxResults: 20,
              },
            },
          },
        },
      ];
