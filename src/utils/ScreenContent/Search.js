export const SearchContents = (props) => [
  {
    viewBlockType: 'container',
    profile: 'container_2',
    title: `Search results for '${decodeURI(props.value)}'`,
    content: {
      preload: [],
      APIFetch: {
        contentType: 'search',
        viewBlockType: 'tile',
        profile: 'tile_1',
        options: {
          part: ['snippet'],
          type: 'video',
          maxResults: 15,
          q: decodeURI(props.value),
        },
      },
    },
  },
];
