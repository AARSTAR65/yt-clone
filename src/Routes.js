import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
  useLocation,
} from 'react-router-dom';
import { AuthContextProvider, useAuthContext } from './context/AuthContext';
import Main from './components/Screens/Main';

function NoMatch() {
  //return <Redirect to="/login"/>
  const location = useLocation();
  return <div>Not found: {location.pathname}</div>;
}

const Logout = () => {
  const auth = useAuthContext();
  React.useEffect(() => {
    auth.logout();
  }, [auth]);
  return <Redirect to="/" />;
};

const HomeRoute = () => <Redirect to="/home" />;

export default function Routes(props) {
  return (
    <AuthContextProvider>
      <Router>
        <Switch>
          <Route exact={true} path="/" component={HomeRoute} />
          <Route
            exact={true}
            path="/:location(home|search|categories)"
            component={Main}
          />
          <Route
            exact={true}
            path="/:location(watch|search|category)/:value"
            component={Main}
          />
          <Route exact={true} path="/logout" component={Logout} />
          {/* <Route path="/location/:var(o1|o2)/:identifierType/:identifierValue?" component={Component} /> */}
          {/* <Route path="/:target(reset|list|details|page|dialog)/:deeplink+" component={DynamicScreen} /> */}
          <Route path="*" component={NoMatch} />
        </Switch>
      </Router>
    </AuthContextProvider>
  );
}
