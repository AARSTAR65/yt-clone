import { Input, makeStyles } from '@material-ui/core';
import { Search } from '@material-ui/icons';
import React from 'react';
import { Link } from 'react-router-dom';
import { shadows } from '../../utils/globaStyles';

const useStyles = makeStyles((theme) => ({
  root: {
    borderRadius: 34,
    display: 'flex',
    alignContent: 'center',
    padding: '5px 5px 5px 20px',
    boxShadow: shadows.embossDepression,
  },
  searchButton: {
    height: 48,
    width: 48,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 24,
    borderRadius: '50%',
    '&:active': {
      boxShadow: shadows.embossDepression,
    },
  },
}));

const SearchBox = () => {
  const classes = useStyles();
  const searchLinkRef = React.useRef({});
  const [searchString, setSearchString] = React.useState('');

  return (
    <div className={classes.root}>
      <Input
        placeholder="Search"
        value={searchString}
        onChange={(event) => setSearchString(event.target.value)}
        inputProps={{
          style: {
            paddingBottom: 3,
            width: 500,
          },
        }}
        onKeyPress={(event) => {
          if (event.key === 'Enter') {
            searchLinkRef.current.click();
          }
        }}
        disableUnderline
        endAdornment={
          <Link
            ref={searchLinkRef}
            to={`/search/${encodeURI(searchString)}`}
            className={classes.searchButton}
          >
            <Search style={{ color: '#3f3e60' }} />
          </Link>
        }
      />
    </div>
  );
};

export default SearchBox;
