import React from 'react';
import Container from './Container';
import Tile from './Tile';

const viewBlockType = {
  container: Container,
  tile: Tile,
};

const ViewController = (props) => {
  const TargetBlock =
    viewBlockType[props.viewBlockType] || (() => <React.Fragment />);
  return <TargetBlock {...props} />;
};

export default ViewController;
