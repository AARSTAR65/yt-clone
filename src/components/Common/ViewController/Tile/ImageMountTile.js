import { makeStyles, Tooltip, Typography } from '@material-ui/core';
import React from 'react';
import { videoIdExtractor } from '../../../../utils/gAPIDataExtractors';
import { shadows } from '../../../../utils/globaStyles';

const useStyles = makeStyles((theme) => ({
  root: {
    margin: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  tileBase: {
    borderRadius: 15,
    width: 275,
    height: 200,
    padding: 0,
    boxShadow: shadows.embossElevation,
    overflow: 'hidden',
    cursor: 'pointer',
    backgroundColor: '#EDF3F9',
    border: 'none',
    '&:active': {
      boxShadow: shadows.embossDepression,
    },
  },
  thumbnail: {
    width: '100%',
    height: 125,
    objectFit: 'cover',
  },
  titleBlock: {
    width: '100%',
    height: 75,
    display: 'flex',
    padding: '15px 15px',
    justifyContent: 'space-around',
    flexDirection: 'column',
    boxSizing: 'border-box',
    alignItems: 'flex-start',
  },
}));

const renderDataProfileTypes = {
  tile_1: (props) => ({
    title: props.snippet.title,
    subtitle: props.snippet.channelTitle,
    imageUrl:
      (props.snippet.thumbnails.standard || {}).url ||
      (props.snippet.thumbnails.medium || {}).url,
    id: videoIdExtractor(props),
    onClick: () => props.history.push(`/watch/${videoIdExtractor(props)}`),
  }),
};

const ImageMountTile = (props) => {
  const classes = useStyles();
  const renderData = (renderDataProfileTypes[props.profile] || (() => ({})))(
    props
  );

  return (
    <Tooltip
      title={renderData.title}
      PopperProps={{ style: { marginTop: 0 } }}
      arrow
    >
      <div className={classes.root}>
        <button onClick={renderData.onClick} className={classes.tileBase}>
          <img
            src={renderData.imageUrl}
            className={classes.thumbnail}
            alt={renderData.title}
          />
          <div className={classes.titleBlock}>
            <Typography
              variant="h6"
              style={{
                maxWidth: 'calc(100% - 30px)',
                overflow: 'hidden',
                color: '#3f3e60',
                whiteSpace: 'nowrap',
                fontSize: '1rem',
                textOverflow: 'ellipsis',
              }}
            >
              {renderData.title}
            </Typography>
            <Typography
              variant="subtitle2"
              style={{
                maxWidth: 'calc(100% - 30px)',
                overflow: 'hidden',
                color: '#909090',
                whiteSpace: 'nowrap',
                textOverflow: 'ellipsis',
              }}
            >
              {renderData.subtitle}
            </Typography>
          </div>
        </button>
      </div>
    </Tooltip>
  );
};

export default ImageMountTile;
