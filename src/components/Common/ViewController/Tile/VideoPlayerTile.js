import { makeStyles, Typography } from '@material-ui/core';
import React from 'react';
import YouTube from 'react-youtube';
import { shadows } from '../../../../utils/globaStyles';

const useStyles = makeStyles((theme) => ({
  root: {
    margin: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  tileBase: {
    borderRadius: 15,
    width: 960,
    height: 660,
    padding: 0,
    boxShadow: shadows.embossElevation,
    overflow: 'hidden',
    cursor: 'pointer',
    backgroundColor: '#EDF3F9',
    border: 'none',
    '&:active': {
      boxShadow: shadows.embossDepression,
    },
  },
  titleBlock: {
    width: '100%',
    height: 75,
    display: 'flex',
    padding: '15px 15px',
    justifyContent: 'space-around',
    flexDirection: 'column',
    boxSizing: 'border-box',
    alignItems: 'flex-start',
  },
  videoPlayerContainer: {
    padding: 0,
  },
}));

const renderDataProfileTypes = {
  tile_2: (props) => ({
    videoId: props.id,
    title: props.snippet.title,
    subtitle: props.snippet.channelTitle,
  }),
};

const VideoPlayerTile = (props) => {
  const classes = useStyles();
  const renderData = (renderDataProfileTypes[props.profile] || (() => ({})))(
    props
  );
  const playerOptions = {
    tileMode: {
      width: '960',
      height: '585',
    },
    theatreMode: {},
  };

  return (
    <div className={classes.root}>
      <button
        onClick={renderData.onClick}
        className={classes.tileBase}
        containerClassName={classes.videoPlayerContainer}
      >
        <YouTube
          opts={playerOptions.tileMode}
          videoId={renderData.videoId}
          id={renderData.videoId}
        />
        <div className={classes.titleBlock}>
          <Typography
            variant="h6"
            style={{
              maxWidth: 'calc(100% - 30px)',
              overflow: 'hidden',
              color: '#3f3e60',
              whiteSpace: 'nowrap',
              fontSize: '1rem',
              textOverflow: 'ellipsis',
              fontWeight: '500',
            }}
          >
            {renderData.title}
          </Typography>
          <Typography
            variant="subtitle2"
            style={{
              maxWidth: 'calc(100% - 30px)',
              overflow: 'hidden',
              color: '#909090',
              whiteSpace: 'nowrap',
              textOverflow: 'ellipsis',
            }}
          >
            {renderData.subtitle}
          </Typography>
        </div>
      </button>
    </div>
  );
};

export default VideoPlayerTile;
