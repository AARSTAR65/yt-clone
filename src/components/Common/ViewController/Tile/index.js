import React from 'react';
import ImageMountTile from './ImageMountTile';
import VideoPlayerTile from './VideoPlayerTile';

const tileTypes = {
  tile_1: ImageMountTile,
  tile_2: VideoPlayerTile,
};

const Tile = (props) => {
  const TargetTile = tileTypes[props.profile] || (() => <React.Fragment />);
  return <TargetTile {...props} />;
};

export default Tile;
