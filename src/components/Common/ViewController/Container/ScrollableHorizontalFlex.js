import React from 'react';
import { makeStyles } from '@material-ui/core';
import { shadows } from '../../../../utils/globaStyles';

const useStyles = makeStyles((theme) => ({
  listContainer: {
    borderRadius: 15,
    boxShadow: shadows.embossDepression,
    padding: 10,
    width: 'calc(100% - 40px)',
    maxWidth: 'calc(100% - 40px)',
    overflowX: 'auto',
    boxSizing: 'border-box',
    display: 'flex',
    alignItems: 'center',
    flexWrap: 'nowrap',
    zIndex: 1,
    margin: '20px 0px',
  },
}));

const ScrollableHorizontalFlex = (props) => {
  const classes = useStyles();

  return <div className={classes.listContainer}>{props.children}</div>;
};

export default ScrollableHorizontalFlex;
