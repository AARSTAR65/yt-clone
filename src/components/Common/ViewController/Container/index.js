import React from 'react';
import { makeStyles, Typography } from '@material-ui/core';
import ScrollableHorizontalFlex from './ScrollableHorizontalFlex';
import EvenlySpacedGrid from './EvenlySpacedGrid';
import ViewController from '..';
import { globalStyles } from '../../../../utils/globaStyles';

const useStyles = makeStyles((theme) => ({
  displayBLock: {
    margin: '0px 0px 20px 0px',
  },
}));

const containerTypes = {
  container_1: ScrollableHorizontalFlex,
  container_2: EvenlySpacedGrid,
};

const Container = (props) => {
  const classes = useStyles();
  const TargetContainer =
    containerTypes[props.profile] || (() => <React.Fragment />);

  return (
    <div className={classes.displayBLock}>
      <Typography variant="h5" style={{ color: globalStyles.colors.dusk }}>
        {props.title}
      </Typography>
      <TargetContainer {...props}>
        {props.content.map((currentItem, index) => (
          <ViewController {...{ ...props, ...currentItem }} />
        ))}
      </TargetContainer>
    </div>
  );
};

export default Container;
