import React from 'react';
import { makeStyles } from '@material-ui/core';
import { shadows } from '../../../../utils/globaStyles';

const useStyles = makeStyles((theme) => ({
  listContainer: {
    display: 'grid',
    gridTemplateColumns: 'auto auto auto auto auto',
    borderRadius: 15,
    boxShadow: shadows.embossDepression,
    padding: 10,
    width: 'calc(100% - 40px)',
    maxWidth: 'calc(100% - 40px)',
    boxSizing: 'border-box',
    zIndex: 1,
    margin: '20px 0px',
    overflowX: 'auto',
  },
}));

const EvenlySpacedGrid = (props) => {
  const classes = useStyles();

  return <div className={classes.listContainer}>{props.children}</div>;
};

export default EvenlySpacedGrid;
