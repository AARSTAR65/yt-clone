import { LinearProgress, Typography } from '@material-ui/core';
import React from 'react';

const LoadingScreen = () => {
  return (
    <div
      style={{
        height: '100vh',
        width: '100vw',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
      }}
    >
      <Typography variant="subtitle2" style={{ color: '#909090' }}>
        Loading...
      </Typography>
      <LinearProgress color="primary" />
    </div>
  );
};

export default LoadingScreen;
