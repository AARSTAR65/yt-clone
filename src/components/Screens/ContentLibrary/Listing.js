import React from 'react';
import viewReducer from '../../../services/viewReducers';
import event, { appEvents } from '../../../utils/eventHandler';
import { CategoriesScreenContents } from '../../../utils/ScreenContent/Categories';
import { HomeScreenContents } from '../../../utils/ScreenContent/Home';
import { MovieTheatreContents } from '../../../utils/ScreenContent/MovieTheatre';
import { SearchContents } from '../../../utils/ScreenContent/Search';
import ViewController from '../../Common/ViewController';

const contentCollection = {
  home: HomeScreenContents,
  categories: CategoriesScreenContents,
  watch: MovieTheatreContents,
  search: SearchContents,
};

const Listing = (props) => {
  const [viewData, setViewData] = React.useState([]);

  const loadRenderData = (loaderArgs = {}) => {
    viewReducer(
      (contentCollection[props.match.params.location] || (() => ({})))({
        ...(props.match.params || {}),
        isLoggedIn: props.isLoggedIn || loaderArgs.isLoggedIn,
      })
    ).then((data) => setViewData([...(data || [])]));
  };

  React.useEffect(() => {
    event.on(appEvents.AUTH_STATUS_CHANGE, loadRenderData);
    loadRenderData();

    return () => {
      event.off(appEvents.AUTH_STATUS_CHANGE, loadRenderData);
    };
  }, [props.match.params.location, props.match.params.value]);

  return (
    <div>
      {(viewData || []).map((viewItem) => (
        <ViewController {...{ ...props, ...viewItem }} />
      ))}
    </div>
  );
};

export default Listing;
