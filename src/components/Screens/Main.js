import {
  AppBar,
  Avatar,
  Button,
  ButtonBase,
  Drawer,
  IconButton,
  makeStyles,
  Typography,
} from '@material-ui/core';
import {
  CategoryRounded,
  ExitToApp,
  HomeRounded,
  Menu,
  Person,
} from '@material-ui/icons';
import React from 'react';
import { globalStyles, shadows } from '../../utils/globaStyles';
import { useAuthContext } from '../../context/AuthContext';
import Listing from './ContentLibrary/Listing';
import { Link } from 'react-router-dom';
import SearchBox from '../Common/SearchBox';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: globalStyles.colors.containerBase,
  },
  drawerOpenPaper: {
    ...globalStyles.desktop.drawer.base,
    width: globalStyles.desktop.drawer.open.width,
  },
  drawerClosedPaper: {
    ...globalStyles.desktop.drawer.base,
    width: globalStyles.desktop.drawer.closed.width,
  },
  appBarDO: {
    ...globalStyles.desktop.header.base,
    width: `calc(100% - ${globalStyles.desktop.drawer.open.width}px)`,
  },
  appBarDC: {
    ...globalStyles.desktop.header.base,
    width: `calc(100% - ${globalStyles.desktop.drawer.closed.width}px)`,
  },
  contentDO: {
    ...globalStyles.desktop.contentArea.base,
    top: globalStyles.desktop.header.base.height,
    left: globalStyles.desktop.drawer.open.width,
    height: `calc(100vh - ${globalStyles.desktop.header.base.height}px)`,
    width: `calc(100% - ${globalStyles.desktop.drawer.open.width}px)`,
  },
  contentDC: {
    ...globalStyles.desktop.contentArea.base,
    top: globalStyles.desktop.header.base.height,
    left: globalStyles.desktop.drawer.closed.width,
    height: `calc(100vh - ${globalStyles.desktop.header.base.height}px)`,
    width: `calc(100% - ${globalStyles.desktop.drawer.closed.width}px)`,
  },
  logoBlock: {
    width: '85%',
    height: globalStyles.desktop.header.base.height,
    display: 'flex',
    padding: globalStyles.desktop.screenBoundaryPadding,
    paddingTop: globalStyles.desktop.screenBoundaryPadding - 7.5,
    alignItems: 'center',
  },
  logo: {
    height: '80%',
  },
  hideTitle: {
    width: 0,
    overflow: 'hidden',
    transition: 'all 0.2s linear',
  },
  profileAvatarCapsule: {
    width: 55,
    height: 60,
    display: 'flex',
    flexWrap: 'nowrap',
    boxShadow: shadows.embossDepression,
    padding: 10,
    boxSizing: 'border-box',
    borderRadius: 30,
    borderTopRightRadius: 0,
    borderBottomRightRadius: 0,
    '&:hover': {
      width: 150,
    },
  },
  profileControlBlock: {
    width: 95,
    height: 50,
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  profileControl: {
    height: 60,
    width: 50,
  },
  menuItemSelected: {
    ...globalStyles.desktop.menuItem.base,
    borderTopRightRadius: 15,
    borderBottomRightRadius: 15,
    boxShadow: shadows.embossDepression,
    '&:hover': {
      width: 'calc(100% - 10px)',
    },
  },
  menuItemIdle: {
    ...globalStyles.desktop.menuItem.base,
  },
}));

const menuItems = [
  {
    title: 'Home',
    route: 'home',
    icon: <HomeRounded style={globalStyles.desktop.menuItem.icon} />,
  },
  {
    title: 'Video Categories',
    route: 'categories',
    icon: <CategoryRounded style={globalStyles.desktop.menuItem.icon} />,
  },
];

const screenIndex = {
  home: Listing,
  categories: Listing,
  watch: Listing,
  search: Listing,
};

const Main = (props) => {
  const classes = useStyles();
  const [menuOpen, setMenuOpen] = React.useState(true);
  const [currentRoute, setCurrentRoute] = React.useState(
    props.match.params.location
  );
  const auth = useAuthContext();
  let CurrentScreen = screenIndex[currentRoute];

  React.useEffect(() => {
    setCurrentRoute(props.match.params.location);
  }, [props.match.params.location]);

  return (
    <div className={classes.root}>
      <AppBar
        elevation={0}
        classes={{
          root: menuOpen ? classes.appBarDO : classes.appBarDC,
        }}
      >
        <IconButton onClick={() => setMenuOpen(!menuOpen)}>
          <Menu />
        </IconButton>
        <SearchBox />
        {!!auth.userData ? (
          <div className={classes.profileAvatarCapsule}>
            <div
              style={{
                width: 140,
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}
            >
              <Avatar
                src={auth.userData.imageURL}
                alt={auth.userData.fullName}
                style={{ filter: shadows.cutout_drop_shadow }}
              >
                <Person />
              </Avatar>
              <div className={classes.profileControlBlock}>
                <ButtonBase
                  className={classes.profileControl}
                  onClick={auth.logout}
                >
                  <ExitToApp
                    style={{
                      fontSize: 40,
                      color: '#3f3e60',
                      filter: shadows.cutout_drop_shadow,
                    }}
                  />
                </ButtonBase>
              </div>
            </div>
          </div>
        ) : (
          <Button
            color="primary"
            variant="contained"
            onClick={auth.login}
            style={{ marginRight: 10 }}
          >
            Sign in
          </Button>
        )}
      </AppBar>
      <Drawer
        variant="permanent"
        open={true}
        ModalProps={{
          keepMounted: true,
        }}
        classes={{
          paper: menuOpen ? classes.drawerOpenPaper : classes.drawerClosedPaper,
        }}
      >
        <div className={classes.logoBlock}>
          <img
            src={`${process.env.PUBLIC_URL}/images/logo.png`}
            alt=""
            className={classes.logo}
          />

          <div {...(menuOpen ? {} : { className: classes.hideTitle })}>
            <Typography
              variant="h5"
              style={{
                fontWeight: 'bold',
              }}
            >
              Youtube
            </Typography>
            <Typography
              variant="subtitle2"
              style={{
                color: '#909090',
                whiteSpace: 'nowrap',
              }}
            >
              Cloned by&nbsp;
              <a
                style={{ textDecoration: 'none' }}
                href="https://linktr.ee/aarstar65"
                target="_blank"
                rel="noreferrer"
              >
                @aastar65
              </a>
            </Typography>
          </div>
        </div>
        <div
          style={{
            marginTop: 30,
          }}
        >
          {menuItems.map((item) => (
            <Link
              to={`/${item.route}`}
              className={
                props.match.params.location === item.route
                  ? classes.menuItemSelected
                  : classes.menuItemIdle
              }
            >
              {item.icon}
              <Typography
                variant="h6"
                style={{
                  overflow: 'hidden',
                  color: '#3f3e60',
                  whiteSpace: 'nowrap',
                  fontSize: '1rem',
                }}
              >
                {item.title}
              </Typography>
            </Link>
          ))}
        </div>
      </Drawer>
      <div className={menuOpen ? classes.contentDO : classes.contentDC}>
        <CurrentScreen
          {...{
            ...props,
            isLoggedIn: !!auth.userData,
          }}
        />
      </div>
    </div>
  );
};

export default Main;
