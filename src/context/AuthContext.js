import React from 'react';
import * as auth from '../services/auth';
import event, { appEvents } from '../utils/eventHandler';

const AuthContext = React.createContext();

function AuthContextProvider({ children }) {
  const [userData, setUserData] = React.useState(null);

  if (!userData) {
    auth.getLoggedInUser().then(setUserData);
  }

  function login() {
    return auth.login().then(setUserData);
  }

  function logout() {
    return auth.logout().then(() => setUserData(null));
  }

  React.useEffect(() => {
    event.emit(appEvents.AUTH_STATUS_CHANGE, {
      userData,
      isLoggedIn: !!userData,
    });
  }, [userData]);

  return (
    <AuthContext.Provider value={{ userData, login, logout }}>
      {children}
    </AuthContext.Provider>
  );
}

function useAuthContext() {
  const auth = React.useContext(AuthContext);
  if (auth === undefined) {
    throw new Error('useAuthContext must be used within a AuthContextProvider');
  }
  return { ...auth, loggedIn: !!auth.userData };
}

function useAuthUserContext() {
  const auth = useAuthContext();
  return auth.userData;
}

export { AuthContextProvider, useAuthContext, useAuthUserContext };
