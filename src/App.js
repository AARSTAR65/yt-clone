import { createMuiTheme, CssBaseline, ThemeProvider } from '@material-ui/core';
import React from 'react';
import LoadingScreen from './components/Common/LoadingScreen';
import Routes from './Routes';
import { initialiseGAPI } from './services/gAPI';
import { globalStyles, shadows } from './utils/globaStyles';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#0000ff',
    },
    secondary: {
      main: '#ff0000',
    },
    background: {
      default: '#EDF3F9',
    },
  },
  overrides: {
    MuiCssBaseline: {
      '@global': {
        '*::-webkit-scrollbar': {
          width: 20,
          height: 20,
          borderRadius: 15,
          boxShadow: shadows.embossDepression,
        },
        '*::-webkit-scrollbar-track': {
          '-webkit-box-shadow': 'inset 0 0 1px rgba(0,0,0,0.00)',
          boxShadow: shadows.embossDepression,
          borderRadius: 15,
        },
        '*::-webkit-scrollbar-thumb': {
          backgroundColor: globalStyles.colors.containerBase,
          boxShadow: shadows.embossElevation,
          borderRadius: 15,
        },
      },
    },
  },
});

function App(props) {
  const [loaded, setLoaded] = React.useState(false);
  React.useEffect(() => initialiseGAPI(setLoaded), [window]);

  return loaded ? (
    <ThemeProvider theme={theme}>
      <CssBaseline>
        <Routes />
      </CssBaseline>
    </ThemeProvider>
  ) : (
    <LoadingScreen />
  );
}

export default App;
