import { config } from '../utils/config';

export const gapi = window.gapi;

export const initialisedGAPI = {
  auth2: {},
};

export const initialiseGAPI = (onInitCallback) => {
  gapi.load('auth2', async () => {
    initialisedGAPI.auth2 = await gapi.auth2.init({
      client_id: config.youtube_api_OAuth_client_id,
      scope: config.GAPI_auth_scope,
    });
    gapi.client.setApiKey(config.youtube_client_api_key);
    await gapi.client.load(
      'https://www.googleapis.com/discovery/v1/apis/youtube/v3/rest'
    );
    onInitCallback(true);
  });
};

export const getVideos = (options) => {
  return gapi.client.youtube.videos.list({
    part: ['snippet'],
    ...(options || {}),
  });
};

export const getSubscriptions = (options) => {
  return gapi.client.youtube.subscriptions.list({
    part: ['snippet'],
    ...(options || {}),
  });
};

export const getPlaylists = (options) => {
  return gapi.client.youtube.playlists.list({
    part: ['snippet'],
    ...(options || {}),
  });
};

export const getPlaylistItems = (options) => {
  return gapi.client.youtube.playlistItems.list({
    part: ['snippet'],
    ...(options || {}),
  });
};

export const getSearchResults = (options) => {
  return gapi.client.youtube.search.list({
    part: ['snippet'],
    ...(options || {}),
  });
};

export const getCollection = {
  videos: getVideos,
  subscriptions: getSubscriptions,
  playlists: getPlaylists,
  playlistItems: getPlaylistItems,
  search: getSearchResults,
};
