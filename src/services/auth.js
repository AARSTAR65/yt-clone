import { initialisedGAPI } from './gAPI';

export async function login() {
  const userAccess = await initialisedGAPI.auth2.grantOfflineAccess();
  if ((userAccess || {}).code) {
    const profile = await initialisedGAPI.auth2.currentUser
      .get()
      .getBasicProfile();

    const userData = {
      accessToken: userAccess.code,
      id: profile.getId(),
      fullName: profile.getName(),
      givenName: profile.getGivenName(),
      familyName: profile.getFamilyName(),
      imageURL: profile.getImageUrl(),
      email: profile.getEmail(),
    };

    await localStorage.setItem('user', JSON.stringify(userData));

    window.location.href = '/';

    return userData;
  } else {
    alert('Something went wrong.');
    return null;
  }
}

export async function logout() {
  await initialisedGAPI.auth2.disconnect();
  await localStorage.removeItem('user');
  window.location.href = '/';
  return 0;
}

export async function getLoggedInUser() {
  // read user data
  const user = JSON.parse(await localStorage.getItem('user'));

  return user;
}
