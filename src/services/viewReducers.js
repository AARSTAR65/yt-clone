import { getCollection } from './gAPI';

const viewBlockReducers = {
  container: containerDataReducer,
  tile: tileDataReducer,
};

async function containerDataReducer({ content, ...baseData }) {
  const { APIFetch } = content || {};
  let processedData = [...((content || {}).preload || [])];
  if (!!APIFetch) {
    let APIResponse = await getCollection[APIFetch.contentType](
      APIFetch.options
    );
    processedData = [
      ...processedData,
      ...(await Promise.all(
        ((APIResponse.result || {}).items || []).map((item) =>
          viewBlockReducers[APIFetch.viewBlockType]({
            viewBlockType: APIFetch.viewBlockType,
            profile: APIFetch.profile,
            ...(item || {}),
          })
        ) || []
      )),
    ];
  }

  return { ...baseData, content: processedData };
}

async function tileDataReducer(tileData) {
  return tileData;
}

async function viewReducer(content) {
  const viewData = await Promise.all(
    content.map((viewItem) =>
      viewBlockReducers[viewItem.viewBlockType](viewItem)
    )
  );

  return viewData;
}

export default viewReducer;
